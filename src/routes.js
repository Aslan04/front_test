import Home from './components/Home.vue';
import Notification from './components/Notification.vue';
import Login from './components/Login.vue';
import News from './components/News.vue';
import Statistic from './components/Statistic.vue';

const routes = [
    { path: '/', component: Home },
    { path: '/notification', component: Notification },
    { path: '/statistic', component: Statistic },
    { path: '/news', component: News},
    { path: '/login', component: Login}
];

export default routes;